CC=gcc
CFLAGS=-c -g
LIBS=-lasound

all: libagcstream.so recorder transcriptstreamagc

recorder: recorder.c
	$(CC) -o recorder recorder.c -L. -lagcstream $(LIBS)

transcriptstreamagc: transcriptstreamagc.c
	$(CC) -o transcriptstreamagc transcriptstreamagc.c -L. -lagcstream -lzmq $(LIBS)

libagcstream.so: agcstream.c agc.c
	$(CC) agcstream.c agc.c -o libagcstream.so -fPIC -shared $(LIBS)

clean:
	rm -rf *.o recorder transcriptstreamagc libagcstream.so


