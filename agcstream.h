/*
 * =====================================================================================
 *
 *       Filename:  streamAGC.h
 *
 *    Description:  header stream agc
 *
 *        Version:  1.0
 *        Created:  19/11/19 15:33:40
 *       Revision:  none
 *       Compiler:  gcc
 * *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <alsa/asoundlib.h>

#define DR_WAV_IMPLEMENTATION
#define MAX_AUDIO_AGC_BUF_LEN   1024*100   //10MB

#include "dr_wav.h"
#include "agc.h"

#ifndef nullptr
#define nullptr 0
#endif

#ifndef MIN
#define  MIN(A, B)        ((A) < (B) ? (A) : (B))
#endif

static void set16le(void *const ptr, const unsigned int value);
static void set32le(void *const ptr, const unsigned int value);
int write_wav_header_s16le(FILE *const out, const int samplesperchannel, const int channels, const int rate);
int agcProcess(int16_t *buffer, uint32_t sampleRate, size_t samplesCount, int16_t agcMode);
int autogain(char *buffer, size_t size);
