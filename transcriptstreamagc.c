/*
 * =====================================================================================
 *
 *       Filename:  transcriptsreamagc.c
 *
 *    Description:  client transcript with agc mode
 *
 *        Version:  1.0
 *        Created:  20/11/19 13:25:48
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */
#include <zmq.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <assert.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdlib.h>
#include <time.h>

#include "agcstream.h"

// Provide random number from 0..(num-1)
#define randof(num) (int) ((float) (num) * random()/ (RAND_MAX + 1.0))

typedef unsigned int    uint32;
typedef unsigned short  unit16;
typedef float           BaseFloat;
typedef int             int32;
typedef char            bool;

#define MAX_AUDIO_BUF_LEN   152428800   //150MB
#define RECV_BUF_LEN        1024000     //1MB

#define false   0
#define true    1

#define REQ_EOS         0
#define REQ_CONNECT     1
#define REQ_AUDIO       2
#define REQ_PING        3
#define REQ_SEP_BEGIN   4
#define REQ_SEP_END     5

#define REP_ACCEPT      1
#define REP_REJECT      0

#define CONNECT_STREAMING   1
#define CONNECT_UPLOAD      2



int main(int argc, char **argv) {

    // Variable
    int seed;
    int portno;
    int rc;
    int response;


    char worker_identity[32];
    char server_addr[32];

    char destination_engine[256];
    char worker_tcp[128];

    char send_buff[256];
    char recv_buff[256];

    char *recv_buffer;
    char *buffer_msg;

    long loops;

    // AGC Variable
    int size;
    int total_size=0;
    int dir;
    unsigned int val;
    unsigned int samplerate=16000; //16Khz

    snd_pcm_t *handle;
    snd_pcm_hw_params_t *params;
    snd_pcm_uframes_t frames;

    char *buffer_recording;

    // Check Parameter
    if (argc < 2){
        printf(" Please use : \" ./trasncriptstreamagc <port-worker-engine> <ip-address-engine> \"\n");
        exit(1);
    }

    // Engine Handler
    portno = atoi(argv[1]);

    // Create Connection to Engine
    void *context = zmq_ctx_new();
    void *worker = zmq_socket (context, ZMQ_DEALER);

    // Create Worker Identity
    seed = time(NULL);
    srand(seed * getpid());

    sprintf(worker_identity, "%04X-%04X", randof(0x10000), randof(0x10000));

    // Set Worker Identity to socket
    zmq_setsockopt(worker, ZMQ_IDENTITY, worker_identity, strlen(worker_identity));

    // Set Memory Allocation for receive message (4 * 102400)
    recv_buffer = (char *) malloc(sizeof(char) * RECV_BUF_LEN);
    if (recv_buffer == NULL) {
        printf("failed to open allocate memory for buffer_msg\n");
        exit(1);
    }

    // Set Address Engine
    sprintf(worker_tcp, "tcp://%s:%d", argv[2], portno);

    // try to connect engine
    printf("Connecting to %s..\n", worker_tcp);
    rc = zmq_connect(worker, worker_tcp);
    assert(rc==0);
    printf("Connected to %s\n", worker_tcp);

    // sent connection engine protocol
    memset(send_buff, 0, 256);
    memset(recv_buff, 0, 256);
    printf("sending REQ_CONNECT to %s\n", worker_tcp);

    send_buff[0] = REQ_CONNECT;
    send_buff[1] = CONNECT_STREAMING;
    zmq_send(worker, send_buff, 2, 0);

    printf("waiting receive ACCEPT from %s\n",worker_tcp);
    rc = zmq_recv(worker, recv_buff, 255, 0);
    if (rc == -1){
        exit(1);
    }
    // Response length > 255
    if (rc > 255) {
        rc = 255;
    }

    recv_buff[rc]=0;

    // Get Data From Receive Buffer
    strncpy(server_addr, &recv_buff[1], 9);

    // If get accept response from engine, continue to record audio
    response = recv_buff[0];
    if (response != REP_ACCEPT) {
        printf("%d : server %s REJECT connection request.\n", response, server_addr);
        exit(1);
    }
    printf("Server %s respond with ACCEPT.\n",server_addr);

    // Preparation for Recording Audio
    /*  Open PCM device for recording (capture).  */
   rc = snd_pcm_open(&handle, "default", SND_PCM_STREAM_CAPTURE, 0);

   if (rc < 0){
       fprintf(stderr, "unable to open pcm device: %s\n", snd_strerror(rc));
       exit(1);
   }

   /*  Allocate a hardware parameters object */
   snd_pcm_hw_params_alloca(&params);

   /*  Fill it in with default vaules.  */
   snd_pcm_hw_params_any(handle, params);

   /*  Set the desired hardware parameters. */
   /* Interleaved mode */
   snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);

   /*  signed 16-bil little-endian format */
   snd_pcm_hw_params_set_format(handle, params, SND_PCM_FORMAT_S16_LE);

    /* Channel -> stereo(2) and mono(1) */
   snd_pcm_hw_params_set_channels(handle, params, 1);

   /* Sampling Rate 16000/48000 Hz*/
   val = 16000;
   snd_pcm_hw_params_set_rate_near(handle, params, &val, &dir);

   /*  Set period size to 32 Frames */
   frames = 32;
   snd_pcm_hw_params_set_period_size_near(handle, params, &frames, &dir);

   /*  Write the parameters to the driver */
   rc = snd_pcm_hw_params(handle, params);
   if( rc < 0 ){
       fprintf(stderr, "unable to set hw parameters: %s\n", snd_strerror(rc));
       exit(1);
   }

   /*  use a buffer large enough to hold one period */
   snd_pcm_hw_params_get_period_size(params, &frames, &dir);

   /*  2 bytes/sample, 1 channels */
   size = frames * 2;

   /*  Allocate Memory (DONT FORGET TO CLEAR!!) */
   buffer_recording=(char*) malloc(size);

   /*  We Want Recording for 5 Seconds */
   snd_pcm_hw_params_get_period_time(params, &val, &dir);
   loops = 5000000 / val;

    printf("DEBUG: 1\n");
    // Set Memory Allocation for buffer audio message
    buffer_msg = (char *) malloc(sizeof(char) * MAX_AUDIO_BUF_LEN);
    if (buffer_msg == NULL) {
        printf("failed to open allocate memory for buffer_msg\n");
        exit(1);
    }

    printf("DEBUG: 2\n");

    // Start Recording Audio
    while (loops > 0) {
        loops--;

   // printf("DEBUG: 3\n");
        buffer_msg[0]=REQ_AUDIO;
        // Recording Audio
        rc = snd_pcm_readi(handle, buffer_recording, frames);
        if (rc == -EPIPE){
            /*  EPIPE means overrun */
            fprintf(stderr, "overrun occurred\n");
            snd_pcm_prepare(handle);
        } else if ( rc < 0 ){
            fprintf(stderr, "error from read: %s\n", snd_strerror(rc));
        } else if ( rc != (int)frames ) {
            fprintf(stderr, "short read, read %d frames\n", rc);
        }
    
        /* AGC PROCESS */
        autogain(buffer_recording, size);

        // Copy data audio to buffer message
        memcpy(&buffer_msg[1], buffer_recording, size);

        // Send to Engine
        zmq_send(worker, buffer_msg, 1+size, 0);

        // Poll every 10 ms for recevie transcript
        zmq_pollitem_t items[]= {
            {worker, 0, ZMQ_POLLIN, 0}
        };
        rc = zmq_poll (items, 1, 10);
        if (rc == -1){
            break; // Interupt
        }

        if (items[0].revents & ZMQ_POLLIN) {
            rc = zmq_recv (worker, recv_buffer, RECV_BUF_LEN,0);
            recv_buffer[rc] = '\0';
            printf("%s\n", recv_buffer);
            if((strncmp(recv_buffer, "FINAL: EOS",10)==0) || (strncmp(recv_buffer, "ERROR:",6)==0)){
                free(recv_buffer);
                break;
            }
        }

        total_size = total_size + size;
    }

    buffer_msg[0] = REQ_EOS;
    zmq_send (worker, buffer_msg, 1, 0);
    printf("sent EOS...\n");

    // Waiting Transcript ALL
    while(1) {
        // Poll every 10 ms for recevie transcript
        zmq_pollitem_t items[]= {
            {worker, 0, ZMQ_POLLIN, 0}
        };
        rc = zmq_poll (items, 1, 10);
        if (rc == -1){
            break; // Interupt
        }

        rc = zmq_recv (worker, recv_buffer, RECV_BUF_LEN,0);
        recv_buffer[rc] = '\0';
        printf("%s\n", recv_buffer);
        if((strncmp(recv_buffer, "FINAL: EOS",10)==0) || (strncmp(recv_buffer, "ERROR:",6)==0)){
            break;
        }

    }


    free(recv_buffer);
    zmq_close (worker);
    zmq_ctx_destroy(context);

    return 0;
}
