/*
 * =====================================================================================
 *
 *       Filename:  recorder.c
 *
 *    Description:  testing library automatic gain control streaming mode
 *
 *        Version:  1.0
 *        Created:  19/11/19 15:26:53
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include "agcstream.h"

#define MAX_BUF_LEN 1024*100

int main(){

   FILE *file_ptr;
   FILE *file_ptr_agc;

   long loops;

   int rc;
   int size;
   int total_size=0;
   int dir;
   unsigned int val;
   unsigned int samplerate=16000; //16Khz

   snd_pcm_t *handle;
   snd_pcm_hw_params_t *params;
   snd_pcm_uframes_t frames;

   char *buffer;
   char *out_buffer;
   int16_t *agc_buffer;

   /*  Open PCM device for recording (capture).  */
   rc = snd_pcm_open(&handle, "default", SND_PCM_STREAM_CAPTURE, 0);

   if (rc < 0){
       fprintf(stderr, "unable to open pcm device: %s\n", snd_strerror(rc));
       exit(1);
   }

   /*  Allocate a hardware parameters object */
   snd_pcm_hw_params_alloca(&params);

   /*  Fill it in with default vaules.  */
   snd_pcm_hw_params_any(handle, params);

   /*  Set the desired hardware parameters. */
   /* Interleaved mode */
   snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);

   /*  signed 16-bil little-endian format */
   snd_pcm_hw_params_set_format(handle, params, SND_PCM_FORMAT_S16_LE);

    /* Channel -> stereo(2) and mono(1) */
   snd_pcm_hw_params_set_channels(handle, params, 1);

   /* Sampling Rate 16000/48000 Hz*/
   val = 16000;
   snd_pcm_hw_params_set_rate_near(handle, params, &val, &dir);

   /*  Set period size to 32 Frames */
   frames = 32;
   snd_pcm_hw_params_set_period_size_near(handle, params, &frames, &dir);

   /*  Write the parameters to the driver */
   rc = snd_pcm_hw_params(handle, params);
   if( rc < 0 ){
       fprintf(stderr, "unable to set hw parameters: %s\n", snd_strerror(rc));
       exit(1);
   }

   /*  use a buffer large enough to hold one period */
   snd_pcm_hw_params_get_period_size(params, &frames, &dir);

   /*  2 bytes/sample, 1 channels */
   size = frames * 2;

   /*  Allocate Memory (DONT FORGET TO CLEAR!!) */
   buffer = (char*) malloc(size);
   out_buffer = (char*) malloc(MAX_BUF_LEN);

   /*  We Want Recording for 5 Seconds */
   snd_pcm_hw_params_get_period_time(params, &val, &dir);
   loops = 5000000 / val;

   /*  Audio File saved */
   file_ptr = fopen("audio-test.wav", "wb");
   file_ptr_agc = fopen("audio-test-agc.wav", "wb");

   /*  Create Header WAV */
   write_wav_header_s16le(file_ptr, MAX_BUF_LEN, 1, 16000);
   write_wav_header_s16le(file_ptr_agc, MAX_BUF_LEN, 1, 16000);

   /*  Set pointer position to 44 on buffer */
   fseek(file_ptr, 44, SEEK_SET);
   fseek(file_ptr_agc, 44, SEEK_SET);

   printf("start recording..\n");
   /*  Start Recording */
   while (loops > 0) {

       loops--;
       rc = snd_pcm_readi(handle, buffer, frames);
       if (rc == -EPIPE){
           /*  EPIPE means overrun */
           fprintf(stderr, "overrun occurred\n");
           snd_pcm_prepare(handle);
       } else if ( rc < 0 ){
           fprintf(stderr, "error from read: %s\n", snd_strerror(rc));
       } else if ( rc != (int)frames ) {
           fprintf(stderr, "short read, read %d frames\n", rc);
       }

        // Write Non AGC
        if (fwrite(buffer, size, 1, file_ptr) != 1) {
      	    fflush(stdout);
      	    fprintf(stderr, "Error writing to standard output.\n");
        break;
        }

        /* Processing AGC */
        //memcpy(agc_buffer, buffer, size);
        // in buffer = buffer audio (int16_t);
        // sample_rate = 16000
        // size = sampleCount
        // agcMode = kAgcModeAdaptiveDigital
        //agcProcess(agc_buffer, samplerate, size, kAgcModeAdaptiveDigital);
        autogain(buffer, size);
        // Write to File
        //if (fwrite((char*)agc_buffer, size, 1, file_ptr_agc) != 1) {
        if (fwrite(buffer, size, 1, file_ptr_agc) != 1) {
      	    fflush(stdout);
      	    fprintf(stderr, "Error writing to standard output.\n");
        break;
        }
        /*  End Of Processing AGC */

        total_size=total_size+size;
        printf("total size : %d\n", total_size);
   }

   // Set Pointer to first position
   fseek(file_ptr, 0, SEEK_SET);
   write_wav_header_s16le(file_ptr, total_size+44, 1, 16000);
   fseek(file_ptr_agc, 0, SEEK_SET);
   write_wav_header_s16le(file_ptr_agc, total_size+44, 1, 16000);

   //Free Memory Allocation
   snd_pcm_drain(handle);
   snd_pcm_close(handle);
   free(buffer);

    return 0;
}
