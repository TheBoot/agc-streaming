## How To Build
----
**dependencies**
- alsa

**Build**
- make clean
- make

## How To Use
----
### Method
```c
int auto_gain(char *buffer, size_t buffer);
```

### Example

```c
/* Recording or Streaming Mode */
#include "agcstream.h"

while(true){

    // AGC Processing
    auto_gain(buffer, size);

    // Write to File if needed
    if (fwrite(out_buffer, size, 1, file_ptr_agc) != 1) {
      	fflush(stdout);
      	fprintf(stderr, "Error writing to standard output.\n");
        break;
    }

}
```
## How To Compile
```
gcc -o yourcode yourcode.c -L. -lagcstream
```

## Sample

1. Recording and save to file
[recorder](http://bhskita.dynu.com:8880/izzul/AGC-library/blob/master/recorder.c)

2. Recording and send audio to engine transcript
[streamagctoengine](http://bhskita.dynu.com:8880/izzul/AGC-library/blob/master/transcriptstreamagc.c)

