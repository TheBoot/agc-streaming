/*
 * =====================================================================================
 *
 *       Filename:  agcstream.c
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  18/11/19 13:10:33
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include "agcstream.h"

/*
This example reads from the default PCM device
and writes to standard output for 5 seconds of data.
*/

static void set16le(void *const ptr, const unsigned int value)
{
    unsigned char *const data = ptr;
    data[0] = value & 255U;
    data[1] = (value >> 8) & 255U;
}

static void set32le(void *const ptr, const unsigned int value)
{
    unsigned char *const data = ptr;
    data[0] = value & 255U;
    data[1] = (value >> 8) & 255U;
    data[2] = (value >> 16) & 255U;
    data[3] = (value >> 24) & 255U;
}

int write_wav_header_s16le(FILE *const out, const int samplesperchannel, const int channels, const int rate)
{
    unsigned char header[44];

    header[0] = 'R';
    header[1] = 'I';
    header[2] = 'F';
    header[3] = 'F';
    set32le(header + 4, 36U + 2U*channels*samplesperchannel);
    header[8] = 'W';
    header[9] = 'A';
    header[10] = 'V';
    header[11] = 'E';
    header[12] = 'f';
    header[13] = 'm';
    header[14] = 't';
    header[15] = ' ';
    set32le(header + 16, 16U);
    set16le(header + 20, 1U); /* PCM format */
    set16le(header + 22, channels);
    set32le(header + 24, rate);
    set32le(header + 28, rate * 2U * channels);
    set16le(header + 32, 2U * channels);
    set16le(header + 34, 16U); /* Bits per sample */
    header[36] = 'd';
    header[37] = 'a';
    header[38] = 't';
    header[39] = 'a';
    set32le(header + 40, 2U*channels*samplesperchannel);

    if (fwrite(header, 44, 1, out) != 1)
        return -1;
    else
        return 0;
}

int agcProcess(int16_t *buffer, uint32_t sampleRate, size_t samplesCount, int16_t agcMode) {
    if (buffer == nullptr) return -1;
    if (samplesCount == 0) return -1;
    WebRtcAgcConfig agcConfig;
    agcConfig.compressionGaindB = 9; // default 9 dB
    agcConfig.limiterEnable = 1; // default kAgcTrue (on)
    agcConfig.targetLevelDbfs = 3; // default 3 (-3 dBOv)
    int minLevel = 0;
    int maxLevel = 255;
    size_t samples = MIN(160, sampleRate / 100);
    if (samples == 0) return -1;
    const int maxSamples = 320;
    int16_t *input = buffer;
    size_t nTotal = (samplesCount / samples);
    void *agcInst = WebRtcAgc_Create();
    if (agcInst == NULL) return -1;
    int status = WebRtcAgc_Init(agcInst, minLevel, maxLevel, agcMode, sampleRate);
    if (status != 0) {
        printf("WebRtcAgc_Init fail\n");
        WebRtcAgc_Free(agcInst);
        return -1;
    }
    status = WebRtcAgc_set_config(agcInst, agcConfig);
    if (status != 0) {
        printf("WebRtcAgc_set_config fail\n");
        WebRtcAgc_Free(agcInst);
        return -1;
    }
    size_t num_bands = 1;
    int inMicLevel, outMicLevel = -1;
    int16_t out_buffer[maxSamples];
    int16_t *out16 = out_buffer;
    uint8_t saturationWarning = 1;                 //是否有溢出发生，增益放大以后的最大值超过了65536
    int16_t echo = 0;                                 //增益放大是否考虑回声影响
    for (int i = 0; i < nTotal; i++) {
        inMicLevel = 0;
        int nAgcRet = WebRtcAgc_Process(agcInst, (const int16_t *const *) &input, num_bands, samples,
                                        (int16_t *const *) &out16, inMicLevel, &outMicLevel, echo,
                                        &saturationWarning);

        if (nAgcRet != 0) {
            printf("failed in WebRtcAgc_Process\n");
            WebRtcAgc_Free(agcInst);
            return -1;
        }
        memcpy(input, out_buffer, samples * sizeof(int16_t));
        input += samples;
    }

    const size_t remainedSamples = samplesCount - nTotal * samples;
    if (remainedSamples > 0) {
        if (nTotal > 0) {
            input = input - samples + remainedSamples;
        }

        inMicLevel = 0;
        int nAgcRet = WebRtcAgc_Process(agcInst, (const int16_t *const *) &input, num_bands, samples,
                                        (int16_t *const *) &out16, inMicLevel, &outMicLevel, echo,
                                        &saturationWarning);
        if (nAgcRet != 0) {
            printf("failed in WebRtcAgc_Process during filtering the last chunk\n");
            WebRtcAgc_Free(agcInst);
            return -1;
        }
        memcpy(&input[samples-remainedSamples], &out_buffer[samples-remainedSamples], remainedSamples * sizeof(int16_t));
        input += samples;
    }

    WebRtcAgc_Free(agcInst);
    return 1;
}

int autogain(char *buffer, size_t size){
    int16_t *agc_buffer;
    int samplerate = 16000; // Default 16Khz / 16000Hz

    // Allocation Memory agc buffer
    agc_buffer = (int16_t*) malloc(MAX_AUDIO_AGC_BUF_LEN);

    memcpy(agc_buffer, buffer, size);

    agcProcess(agc_buffer, samplerate, size, kAgcModeAdaptiveDigital);

    memcpy(buffer, agc_buffer, size);

    // Free Memory
    free(agc_buffer);

    return 0;
}

